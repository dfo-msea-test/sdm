###############################################################################
#
# Authors:      Jessica Nephin, Cole Fields, Sarah Davies,
#               Matthew H. Grinnell and Jessica Finney
# Affiliation:  Fisheries and Oceans Canada (DFO)
# Group:        Marine Spatial Ecology and Analysis
# Location:     Institute of Ocean Sciences
# Contact:      e-mail: jessica.nephin@dfo-mpo.gc.ca | tel: 250.363.6564
#
# Overview:
# Calls functions from SDM_Functions.R that build and validate generalized
# linear models (GLM) or boosted regression tree (BRT) models and predict
# habitat suitability or species abundance.
#
# Notes:
# Run using RunSDM.R
#
###############################################################################


########################
#####  Directories  ####
########################

# Remove the old species directory if present
if(spp %in% list.files(path = outdir)) unlink(file.path(outdir, spp), recursive=TRUE)

# Create base directories
dir.create( file.path(outdir, spp) )



#######################
#####  Load Env   #####
#######################
# created in Env2Preds.R

# Load environmental data
load( file=file.path(indir, "Data/EnviroDat.RData") )

# Load environmental layer
load( file=file.path(indir, "Data/EnviroLayers.RData") )



#######################
#####   sppList   #####
#######################

# Make Species Data List
sppEnvList <- MakeSpeciesDataList( obsDat=obsList, envDat=enviroDat )

# Get species observations and environmental data
sppList <- sppEnvList$obs
enviroDat <- sppEnvList$env$envDat

# Load CV list for species
cv <- cv.list[[spp]]$cv

# Number of CV folds
numFolds <- length(cv)




###############################
#####   Model Selection   #####
###############################

# Calculate autocorrelation in the response data
ObsVario <- CalcVario( dat=sppList$spdf@data[,1], Obs = TRUE )

# loop through model type
# Run once for Env model and once for Env+SAC model
for (outType in type){

  # Run initial Env model
  if( outType == "Env"){

    # Set up directories
    dir.create( file.path(outdir, spp, outType) )
    dir.create( file.path(outdir, spp, outType, outData) )
    dir.create( file.path(outdir, spp, outType, outFigs) )
    dir.create( file.path(outdir, spp, outType, outTabs) )
    if ( modtype == "BRT" ) dir.create( file.path(outdir, spp, outType, outTune) )

    if ( modtype == "GLM" ){
      # Run glm selection
      cat( "\nSelecting the best GLM(s)...\n" )
      model.list <- lapply( 1:numFolds, glmfunction )
    } else if ( modtype == "BRT" ) {
      # Run the tuning procedure
      tune <- RunTuneBRT( loc=file.path( outdir, spp, outType, outTune), tol=tolerance )
      # Get the optimal BRT parameters
      optParslist <- lapply( 1:numFolds, GetBestPars, mat=tune )
      # Load the optimal BRT
      model.list <- lapply( 1:numFolds, LoadBestBRT, pars=optParslist )
    }

    # Calculate mean residual values
    resids <- CalcResid( )

    # Calculate mean residual spatial autocorrelation from models
    ResVario <- CalcVario( dat=resids )

    # Is residual positive spatial autocorrelation significant ?
    isSAC <- ResVario$GearyC$p.value < SACThresh
  }

  # Re-run with SAC if residual spatial autocorrelation is significant
  if( isSAC & outType == "Env-SAC" ) {

    # Set up directories
    dir.create( file.path(outdir, spp, outType) )
    dir.create( file.path(outdir, spp, outType, outData) )
    dir.create( file.path(outdir, spp, outType, outFigs) )
    dir.create( file.path(outdir, spp, outType, outTabs) )
    if ( modtype == "BRT" ) dir.create( file.path(outdir, spp, outType, outTune) )

    # Message
    cat( "\nResidual Geary C p-value < ", SACThresh, ": calculating autocovariate... \n", sep="" )
    # Calculate autocovariate
    SAC <- CalcAutoCov( neighDist=ObsVario$neighDist )


    if ( modtype == "GLM" ){
      # re-run glms with spatial autocovariate (SAC) for each CV fold
      cat( "\nRe-runing GLM with autocovariate...", sep="" )
      model.list <- lapply( 1:numFolds, glmSAC )
    } else if ( modtype == "BRT" ) {
      # Run BRT model fitting with set tunning parameters from Env models
      model.list <- lapply( 1:numFolds, brtSAC,
                            loc=file.path( outdir, spp, outType, outTune),
                            tol=tolerance)
    }

    # Calculate mean residual values
    resids <- CalcResid( )

    # Calculate residual spatial autocorrelation
    ResVario <- CalcVario( dat=resids )

  } else if( !isSAC & outType == "Env-SAC" ) {
    # If SAC is not significant, no "Env-SAC" model run
    message("\nWarning: No residual autocorrelation, Env+SAC model not run")
    stop("noSAC")
  }


  ######################
  #####  Evaluate  #####
  ######################

  # Run evaluation function over folds
  evalstats <- lapply( 1:numFolds, evalStats )
  # Mean and SD threshold independant stats
  allstats <- NULL
  for( i in 1:numFolds)  allstats <- rbind(allstats, evalstats[[i]]$stats)
  mean.stats <- apply( allstats, 2, mean )
  sd.stats <- apply( allstats, 2, sd )
  # Messages
  cat( "Mean threshold independant statistics for testing data:\n",
       "% deviance explained=", round( mean.stats["test.PDevExp"],2),
       ", AUC=", round( mean.stats["test.AUC"],2),
       "\n\n", sep="" )

  # If binomial  | bernoulli, calcualte threshold and stats
  if( sppList$famDist == "binomial"  | sppList$famDist == "bernoulli" ) {
    threshstats <- lapply( 1:numFolds, CalcThreshPA, stat="Informedness")
    # Mean and SD threshold dependant stats
    allthresh <- NULL
    for( i in 1:numFolds)  allthresh <- rbind(allthresh, threshstats[[i]]$stats)
    mean.threshstats <- apply( allthresh, 2, mean )
    sd.threshstats <- apply( allthresh, 2, sd )
    # Messages
    cat( "Mean threshold dependant statistics for testing data:\n",
         "Threshold=", round(mean.threshstats["test.Thresh"], 2),
         " (kappa=", round( mean.threshstats["test.Kappa"],2),
         ", TSS=", round( mean.threshstats["test.TSS"], digits=2),
         ", accuracy=", round( mean.threshstats["test.Acc"], digits=2),
         ")\n\n", sep="" )
  }

  # Calulcate mean predictions
  trainpreds <- meanPreds( type = "train" )
  testpreds <- meanPreds( type = "test" )

  # Run variable improtance function over folds
  if ( modtype == "GLM" ){
    relimp <- varImpGLM( )
  } else if ( modtype == "BRT" ) {
    relimp <- varImpBRT( )
  }

  # run marginal effects function over folds
  margs <- marginaleffects( )


  # Update the species summary table with evaluation statistics: train
  ind <- which(spTable$Species == spp & spTable$ModelType == outType & spTable$Data == "Train")
  # Update the species summary table with evaluation statistics
  spTable[ind, "PDevExp.mean"] <- round( mean.stats[['train.PDevExp']], 2 )
  spTable[ind, "PDevExp.sd"] <- round( sd.stats[['train.PDevExp']], 2 )
  spTable[ind, "AUC.mean"] <- round( mean.stats[['train.AUC']], 2 )
  spTable[ind, "AUC.sd"] <- round( sd.stats[['train.AUC']], 2 )
  spTable[ind, "r2.mean"] <- round( mean.stats[['train.r2']], 2 )
  spTable[ind, "r2.sd"] <- round( sd.stats[['train.r2']], 2 )
  # If PA
  if( sppList$famDist == "binomial"  | sppList$famDist == "bernoulli" ) {
    spTable[ind, "Threshold.mean"] <- round(mean.threshstats["train.Thresh"], 2)
    spTable[ind, "Threshold.sd"] <- round(sd.threshstats["train.Thresh"], 2)
    spTable[ind, "Kappa.mean"] <- round( mean.threshstats["train.Kappa"],2)
    spTable[ind, "Kappa.sd"] <- round( sd.threshstats["train.Kappa"],2)
    spTable[ind, "TSS.mean"] <- round( mean.threshstats["train.TSS"], digits=2)
    spTable[ind, "TSS.sd"] <- round( sd.threshstats["train.TSS"], digits=2)
    spTable[ind, "Accuracy.mean"] <- round( mean.threshstats["train.Acc"], digits=2)
    spTable[ind, "Accuracy.sd"] <- round( sd.threshstats["train.Acc"], digits=2)
    spTable[ind, "Sensitivity.mean"] <- round( mean.threshstats["train.Sens"], digits=2)
    spTable[ind, "Sensitivity.sd"] <- round( sd.threshstats["train.Sens"], digits=2)
    spTable[ind, "Specificity.mean"] <- round( mean.threshstats["train.Spec"], digits=2)
    spTable[ind, "Specificity.sd"] <- round( sd.threshstats["train.Spec"], digits=2)
  }

  # Update the species summary table with evaluation statistics: test
  ind <- which(spTable$Species == spp & spTable$ModelType == outType & spTable$Data == "Test")
  # Update the species summary table with evaluation statistics
  spTable[ind, "PDevExp.mean"] <- round( mean.stats[['test.PDevExp']], 2 )
  spTable[ind, "PDevExp.sd"] <- round( sd.stats[['test.PDevExp']], 2 )
  spTable[ind, "AUC.mean"] <- round( mean.stats[['test.AUC']], 2 )
  spTable[ind, "AUC.sd"] <- round( sd.stats[['test.AUC']], 2 )
  spTable[ind, "r2.mean"] <- round( mean.stats[['test.r2']], 2 )
  spTable[ind, "r2.sd"] <- round( sd.stats[['test.r2']], 2 )
  #If PA
  if( sppList$famDist == "binomial"  | sppList$famDist == "bernoulli" ) {
    spTable[ind, "Threshold.mean"] <- round(mean.threshstats["test.Thresh"], 2)
    spTable[ind, "Threshold.sd"] <- round(sd.threshstats["test.Thresh"], 2)
    spTable[ind, "Kappa.mean"] <- round( mean.threshstats["test.Kappa"],2)
    spTable[ind, "Kappa.sd"] <- round( sd.threshstats["test.Kappa"],2)
    spTable[ind, "TSS.mean"] <- round( mean.threshstats["test.TSS"], digits=2)
    spTable[ind, "TSS.sd"] <- round( sd.threshstats["test.TSS"], digits=2)
    spTable[ind, "Accuracy.mean"] <- round( mean.threshstats["test.Acc"], digits=2)
    spTable[ind, "Accuracy.sd"] <- round( sd.threshstats["test.Acc"], digits=2)
    spTable[ind, "Sensitivity.mean"] <- round( mean.threshstats["test.Sens"], digits=2)
    spTable[ind, "Sensitivity.sd"] <- round( sd.threshstats["test.Sens"], digits=2)
    spTable[ind, "Specificity.mean"] <- round( mean.threshstats["test.Spec"], digits=2)
    spTable[ind, "Specificity.sd"] <- round( sd.threshstats["test.Spec"], digits=2)
  }


  ###################
  ##### Predict #####
  ###################

  if( outType == "Env" ) {

    # Predict species distribution
    predSDM <- PredictSDM(  )

    # Indicate if environmental variables are outside the range of observed data
    extrapLayer <- CalcExtrapRange( xyLayers=enviroLayers,
                                    obs=enviroDat@data )
    
    # Convert residuals to raster for plotting
    residualRaster <- aggResiduals( ptData = sppList$spdf, residuals = resids, 
                                    rasterLayer = predSDM[[1]],
                                    by=aggby )

    # Calculate presence/absence predicted layer (using training data)
    if( sppList$famDist == "binomial"  | sppList$famDist == "bernoulli" ) {
      presabs <- CalcPresAbs( thresh=mean.threshstats["train.Thresh"],
                                  meanpreds=trainpreds, env=predSDM )
    }

    # Calculate low, medium and high density (if non-PA)
    if( !(sppList$famDist %in% c("binomial","bernoulli")) ) {
      predSDM <- CalcLowMedHigh( env=predSDM )
    }

    # Mask predicted layer with extrapolated layer to show greater variation in predicted map
    maskedPredSDM <- ConditionalMask(predSDM, extrapLayer, 1)
  }

  # Garbage Collection
  gc(reset=TRUE)



  ##################
  #####  Save  #####
  ##################

  #--------------------------#
  # Save the predicted layers
  if( outType == "Env" ) {
    save( predSDM, file=file.path( outdir, spp, outType, outData,
                                   "predSDM.RData") )
    # Export predicted layers: polygon
    if( inEnvType == "polygon" ){
      writeOGR( predSDM, dsn = file.path(outdir, spp, outType, outData),
                layer = "predSDM", driver = "ESRI Shapefile", overwrite_layer = TRUE)
    }
    # Export predicted binned layers: raster
    if( inEnvType == "raster" ) {
      writeRaster( x=residualRaster, overwrite=TRUE,
                   filename=file.path( outdir, spp, outType, outData,
                                       "aggregatedResiduals.tif" ))
      
      if( sppList$famDist == "binomial"  | sppList$famDist == "bernoulli" ){
        writeRaster( x=predSDM[["PredPA"]], overwrite=TRUE,
                     filename=file.path( outdir, spp, outType, outData,
                                         "predSDM_PA.tif" ))
      }
      if( !(sppList$famDist %in% c("binomial","bernoulli")) ){
        writeRaster( x=predSDM[["PredBin"]], overwrite=TRUE,
                     filename=file.path( outdir, spp, outType, outData,
                                         "predSDM_predbin.tif" ))
      }
    } # End if raster
  } # End if Env

  #------------------------#
  # Save extrapolated layer
  if( outType == "Env" ){
    save( extrapLayer, file=file.path( outdir, spp, outType, outData,
                                       "extrap.RData") )
    # Export extrapolated layer: polygon
    if( inEnvType == "polygon" ){
      writeOGR( extrapLayer, dsn=file.path( outdir, spp, outType, outData),
                layer="extrap", driver = "ESRI Shapefile", overwrite_layer = TRUE )
    }
    # Export extrapolated layer: raster
    if( inEnvType == "raster" ) {
      # Save extrapolated layer in species-specific database: raster
      writeRaster( x=extrapLayer,
                   filename=file.path( outdir, spp, outType, outData,
                                       "extrap.tif") )
    } # End if raster
  } # End if env

  #---------------------------------#
  # Save the masked predicted layers
  if( outType == "Env" ) {
    save( maskedPredSDM, file=file.path( outdir, spp, outType, outData,
                                         "maskedPredSDM.RData") )
    # Export masked layers: polygon
    if( inEnvType == "polygon" ){
      writeOGR( maskedPredSDM, dsn = file.path(outdir, spp, outType, outData),
                layer = "maskedPredSDM", driver = "ESRI Shapefile", overwrite_layer = TRUE)
    }
    # Export masked binned layers: raster
    if( inEnvType == "raster" ) {
      writeRaster( x=maskedPredSDM[["Predicted"]], overwrite=TRUE,
                   filename=file.path( outdir, spp, outType, outData,
                                       "maskedPredSDM_predicted.tif" ))
      if( sppList$famDist == "binomial"  | sppList$famDist == "bernoulli" ){
        writeRaster( x=maskedPredSDM[["PredPA"]], overwrite=TRUE,
                     filename=file.path( outdir, spp, outType, outData,
                                         "maskedPredSDM_PA.tif" ))
      }
      if( !(sppList$famDist %in% c("binomial","bernoulli")) ){
        writeRaster( x=maskedPredSDM[["PredBin"]], overwrite=TRUE,
                     filename=file.path( outdir, spp, outType, outData,
                                         "maskedPredSDM_predbin.tif" ))
      }
    } # End if raster
  } # End if Env

  #-----------------------------#
  # Save SAC environmental layer
  if( isSAC & outType == "Env-SAC" ){
    save( SAC, file=file.path( outdir, spp, outType, outData, "SAC.RData") )

    # Export SAC environmental layer: polygon
    if( inEnvType == "polygon" ){
      writeOGR( SAC, dsn = file.path( outdir, spp, outType, outData), layer = "SAC",
                driver = "ESRI Shapefile", overwrite_layer =TRUE)
    }
    # Export SAC environmental: raster
    if( inEnvType == "raster" ) {
      writeRaster( x=SAC, filename=file.path( outdir, spp, outType, outData,
                                              "SAC.tif"), overwrite=TRUE )
    } # End if raster
  } # End if Env-SAC


  # Garbage Collection
  gc(reset=TRUE)


  ##################
  ##### Tables #####
  ##################

  # Write evaluation stats from each fold to csv
  write.csv( allstats,
             file=file.path( outdir, spp, outType, outTabs, "evalStats.csv"),
             row.names=TRUE )
  # Write threshold stats from each fold to csv
  if( sppList$famDist == "binomial"  | sppList$famDist == "bernoulli" ) {
    write.csv( allthresh, file=file.path( outdir, spp, outType, outTabs, "thresholdStats.csv"),
               row.names=TRUE )
  }
  # Write BRT tuning table to csv
  if ( modtype == "BRT" ) {
    write.csv( x=tune,
               file=file.path( outdir, spp, outType, outTabs, "tune.csv"),
               row.names=FALSE )
  }

  # Write species data to csv: observations
  write.csv( x=cbind(sppList$spdf@coords, sppList$spdf@data),
             file=file.path( outdir, spp, outType, outTabs, "sppListSPDF.csv"),
             row.names=FALSE )

  # Write variable importance to csv
  write.csv( x=relimp,
             file=file.path( outdir, spp, outType, outTabs, "relVarImp.csv"),
             row.names=FALSE )

  # Write variograms to csv
  write.csv( x=data.frame(Semivariance=ResVario$Vario$gamma,Distance=ResVario$Vario$dist),
             file=file.path( outdir, spp, outType, outTabs, "ResVario.csv"),
             row.names=FALSE )
  if( outType == "Env" ) write.csv(
    x=data.frame(Semivariance=ObsVario$Vario$gamma,Distance=ObsVario$Vario$dist),
    file=file.path( outdir, spp, outType, outTabs, "ObsVario.csv"),
             row.names=FALSE )


  ###################
  ##### Figures #####
  ###################

  # Plot observed species versus predictors
  # Scatterplot matrices: observed
  PlotPredObs( y=sppList$spdf@data[[spp]], preds=enviroDat@data,
               yLab="Observed", preFix="Observed" )

  # Plot mean fitted values versus predictors
  # Scatterplot matrices: mean fitted values
  if( sppList$famDist == "binomial"  | sppList$famDist == "bernoulli" ) {
    PlotPredObs( y=presabs, preds=enviroDat@data,
                 yLab="Fitted Values", preFix="Fitted")
  } else {
    PlotPredObs( y=trainpreds, preds=enviroDat@data,
                 yLab="Fitted Values", preFix="Fitted")
  }

  # Plot mean residuals versus predictors
  # Scatterplot matrices: mean residuals
  PlotPredRes( y=resids, preds=enviroDat@data,
               Observed=sppList$spdf@data[[spp]], preFix="Residual" )

  # Plot observed and mean fitted and residual values
  if( !(sppList$famDist %in% c("binomial","bernoulli")) ){
    PlotObsPredResid( obs=sppList$spdf@data[[spp]],
                      pred=trainpreds,
                      resid=resids, prefix="" )
  }

  # Plot observed and predicted classes
  if( sppList$famDist == "binomial"  | sppList$famDist == "bernoulli" ){
    PlotObsPredClass( type="train" )
    PlotObsPredClass( type="test" )
  }

  # Plot histogram of mean prediction ( not including extrapolated areas )
  PlotPredHist( x=maskedPredSDM )

  # Plot variable importance
  PlotRelVarImp( relimp )

  # run marginal effects plotting function
  PlotMargEff( marg=margs, prefix="MarginalEffects", figExt=".pdf" )


  #Plot Accuracy over threshold ranges (if required)
  if( sppList$famDist == "binomial"  | sppList$famDist == "bernoulli" ) {
    PlotAccuracy( type="train" )
    PlotAccuracy( type="test" )
  }

  # Plot receiver operating characteristic curve (if required)
  if( sppList$famDist == "binomial"  | sppList$famDist == "bernoulli" ) {
    PlotROC( type="train" )
    PlotROC( type="test" )
  }

  # Plot variograms (Residual and Observed)
  PlotVariogram( dat=ResVario, type = "Residual" )
  if( outType == "Env" ) PlotVariogram( dat=ObsVario, type = "Observed" )


  # Plot BRT performance statistic vs tuning parameters for each fold
  if ( modtype == "BRT" & outType == "Env" & length(learnRate) > 1 ){
    for( f in 1:numFolds) PlotPerfTune( fold=f, dat=tune, pars=optParslist,
                                        mod=model.list )
  }

  # Calculate x,y extent for maps
  lims <- getLims( Layer = predSDM )
  
  # Map of Observations
  if( outType == "Env" ) MapObs( ptData=sppList$spdf, lims=lims, Obs=TRUE, legendPos=legendPos )

  # Map of model residuals
  MapObs( ptData=sppList$spdf, lims=lims, Resid=TRUE, legendPos=legendPos )
  
  # Map of model residuals, converted to raster and aggregated (raster resolution * factor of 100)
  if( outType == "Env" ) MapResiduals( residualsRaster = residualRaster, 
                                       lims=lims, legendPos=legendPos )

  # Map of Predictions
  if( outType == "Env" ) MapLayers( layers=predSDM, lims=lims, facVars=c("PredBin","PredPA"), 
                                    mcpoly=TRUE, legendPos=legendPos )

  # Map of Masked Predictions
  if( outType == "Env" ) MapLayers( layers=maskedPredSDM, lims=lims, facVars=c("PredBin","PredPA"),
                                    prefix="MaskedMap_", mcpoly=TRUE, legendPos=legendPos )

  # Map of Autocovariate
  if(  isSAC & outType == "Env-SAC" ) {
    MapObs( ptData=sppList$spdf, lims=lims, SAC=TRUE, legendPos=legendPos )
    MapLayers( layers=SAC, lims=lims, style="kmeans",facVars=facVars, legendPos=legendPos )
  }

  # Map of extrapolated areas
  if( outType == "Env" ) MapLayers( layers=extrapLayer, lims=lims, facVars="extrap", 
                                    mcpoly=TRUE, legendPos=legendPos )

  # Save the workspace image
  save.image( file=file.path( outdir, spp, outType, paste0(outType,"_Image.RData") ) )

} # End model loop
