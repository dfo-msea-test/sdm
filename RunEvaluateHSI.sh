#!/c/cygwin64/bin/bash
# Run in command line from parent directory
# bash Run.sh
# Update controls and paramters in Evaluate.R script
# -----------------------------------------------------#

# loop through projects
# evaluate HSI models
for sp in Abalone Geoduck Pterygophora Urchin Zostera Crabs Rockfishes
do
  # Run Data prep
  if [ "$sp" == "Abalone" ]
  then
    Rscript --vanilla --verbose Evaluate.R $sp ABL_PA
  fi
  # Run Data prep
  if [ "$sp" == "Geoduck" ]
  then
    Rscript --vanilla --verbose Evaluate.R $sp GDK_PA
  fi
  # Run Data prep
  if [ "$sp" == "Pterygophora" ]
  then
    Rscript --vanilla --verbose Evaluate.R $sp PT_PA
  fi
  # Run Data prep
  if [ "$sp" == "Urchin" ]
  then
    Rscript --vanilla --verbose Evaluate.R $sp RSU_PA
  fi
  # Run Data prep
  if [ "$sp" == "Zostera" ]
  then
    Rscript --vanilla --verbose Evaluate.R $sp ZO_PA
  fi
  # Run Data prep
  if [ "$sp" == "Crabs" ]
  then
    Rscript --vanilla --verbose Evaluate.R $sp CRAB_PA
  fi
  # Run Data prep
  if [ "$sp" == "Rockfishes" ]
  then
    Rscript --vanilla --verbose Evaluate.R $sp S_maliger_PA
    Rscript --vanilla --verbose Evaluate.R $sp S_ruberrim_PA
  fi
done
