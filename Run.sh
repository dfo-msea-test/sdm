#!/c/cygwin64/bin/bash
# Run in command line from parent directory
# bash Run.sh
# Update controls and paramters in DataPrep.R,
#    RunSDM.R and Ensemble.R scripts before running
# -----------------------------------------------------#

# # loop through projects
for sp in Pterygophora Zostera Crab Rockfish
do
  # Run Data prep
  if [ "$sp" == "Pterygophora" ]
  then
    # Run Data prep
    Rscript --vanilla --verbose DataPrep.R $sp PT
    # Run GLM
    Rscript --vanilla --verbose RunSDM.R $sp GLM
    # Run BRT
    Rscript --vanilla --verbose RunSDM.R $sp BRT
    # Run Ensemble_noHSI
    Rscript --vanilla --verbose Ensemble.R $sp PT_PA
  fi
  # Run Data prep
  if [ "$sp" == "Zostera" ]
  then
    # Run Data prep
    Rscript --vanilla --verbose DataPrep.R $sp ZO
    # Run GLM
    Rscript --vanilla --verbose RunSDM.R $sp GLM
    # Run BRT
    Rscript --vanilla --verbose RunSDM.R $sp BRT
    # Run Ensemble_noHSI
    Rscript --vanilla --verbose Ensemble.R $sp ZO_PA
  fi
  if [ "$sp" == "Crab" ]
  then
  # Run Data prep
  Rscript --vanilla --verbose DataPrep.R $sp CRAB
  # Run GLM
  Rscript --vanilla --verbose RunSDM.R $sp GLM
  # Run BRT
  Rscript --vanilla --verbose RunSDM.R $sp BRT
  # Run Ensemble_noHSI
  Rscript --vanilla --verbose Ensemble.R $sp CRAB_PA
  fi
  # Run Data prep
  if [ "$sp" == "Rockfish" ]
  then
    # Run Data prep
    Rscript --vanilla --verbose DataPrep.R $sp S_maliger-S_ruberrim
    # Run GLM
    Rscript --vanilla --verbose RunSDM.R $sp GLM
    # Run BRT
    Rscript --vanilla --verbose RunSDM.R $sp BRT
    # Run Ensemble_noHSI
    Rscript --vanilla --verbose Ensemble.R $sp S_maliger_PA
    Rscript --vanilla --verbose Ensemble.R $sp S_ruberrim_PA
  fi
  done
