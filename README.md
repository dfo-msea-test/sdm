## Input data requirements

**Observations**
*  A single shapefile with point or line data
*  Located in Projects/*project-name*/Data/SpeciesData/ directory
*  Will be projected to the CRS of the environmental data

**Predictors**
*  Categorical data needs to be represented by numerical values (starting with 1, not 0)
*  Accepts data as raster or polygon (not fully implemented), either:
  1)  Multiple raster files
      *  Located in Projects/*project-name*/Data/EnvData/Rasters/ directory
      *  Either .tif or .asc formats
  2)  A single geodatabase with a polygon feature class
      *  Located in Projects/*project-name*/Data/EnvData/Polygons/ directory
      *  Environmental parameters are attribute fields
* The use of polygon predictor data is not fully supported and still in development (using polygons may results in errors)


## Running the model
All scripts described below run from the ~/Scripts working directory.

**Run DataPrep.R**
  *  Processes the observations and predictor data prior to modelling
  *  Before executing, adjust 'Control' options within DataPrep.R
  *  Output will be placed in a new directory Projects/*project-name*/DataPrep/

**Run RunSDM.R**
  *  Fits, evaluates and predicts (GLM and BRT options currently supported)
  *  Before executing, adjust 'Control' and 'Parameter' options within RunSDM.R
  *  Output will be place in a new directory Projects/*project-name*/Models/*model-type*/

**Run Evaluate.R**
  *  Evaluates models not build with RunSDM.R (e.g. knowledge-based model) using data from DataPrep.R
  *  Before executing, adjust 'Control' options within Evaluate.R
  *  Output will be place in an existing directory Projects/*project-name*/Models/*model-type*/

**Run Ensemble.R**
  *  Builds an ensemble model from models developed from RunSDM.R (e.g. GLM and BRT)
  *  Calculates the difference between the ensemble and a knowledge-based model (e.g. HSI) if available
  *  Before executing, adjust 'Control' options within Ensemble.R
  *  Output will be place in a new directory Projects/*project-name*/Models/Ensemble/

**Run.sh/RunEvaluateHSI.sh**
  *  Loops through multiple projects calling scripts via the Rscript command line tool


## Initial Directory Structure
*  If the name is within <>, it can be changed
*  Create new project folder for each new modelling project

```
/---<SDM>   
    /---Scripts   
    |   
    /---Documentation    
    |         
    /---Projects   
        /---<project-name>   
            /---Coastline   
            |       <polygon-name>.shp   
            |          
            /---Data   
                /---EnvData   
                |   /---Polygons   
                |   |       <gdb-name>.gdb   
                |   |           
                |   /---Rasters   
                |           <raster1-name>.tif   
                |           <raster2-name>.tif   
                |           
                /---SpeciesData   
                        <obs-name>.shp   
```

## Citation

Nephin, J., E.J. Gregr, C. St. Germain, C. Fields, J.L. Finney. 2020. Development of a Species Distribution Modelling Framework and its Application to Twelve Species on Canada’s Pacific Coast. DFO Can. Sci. Advis. Sec. Res. Doc. In press.

## Contact

Jessica Nephin   
Affiliation:  Fisheries and Oceans Canada (DFO)   
Group:        Marine Spatial Ecology and Analysis   
Location:     Institute of Ocean Sciences   
Contact:      e-mail: jessica.nephin@dfo-mpo.gc.ca | tel: 250.363.6564   
