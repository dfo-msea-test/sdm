###############################################################################
#
# Authors:      Jessica Nephin, Cole Fields, Sarah Davies,
#               Matthew H. Grinnell and Jessica Finney
# Affiliation:  Fisheries and Oceans Canada (DFO)
# Group:        Marine Spatial Ecology and Analysis
# Location:     Institute of Ocean Sciences
# Contact:      e-mail: jessica.nephin@dfo-mpo.gc.ca | tel: 250.363.6564
# Project:      SpeciesDistributionModels
#
# Overview:
# Aggregate species observations if needed. Extract environmental data at the
# location of species observations. Examine collinearity between environmental
# predictors. Export species and env. data to be used in RunSDM.R for modelling.
#
# Requirements:
# *  DataPrep_Functions.R
# *  Species density or presense absence data by location (x, y) as a shapefile,
#    environmental layers as rasters (.tif or .asc) or polygons (.gdb).
#
###############################################################################


########################
##### Housekeeping #####
########################

# Move back to parent directory
setwd('..')

# General options
rm( list=ls( ) )          # Clear the workspace
options( scipen=999 )     # Remove scientific notation

# source functions
source("Scripts/DataPrep_Functions.R")


# Load required packages
UsePackages( c("raster", "sp", "rgdal", "usdm", "adehabitatHR",
               "maptools", "ggplot2", "GGally", "gridExtra",
               "dplyr", "reshape2", "RColorBrewer", "rgeos") )


# install blockCV package from github if needed and load
if("blockCV" %in% rownames(installed.packages()) == FALSE) {
  devtools::install_github("rvalavi/blockCV")
  library("blockCV")
} else {
  library("blockCV")
}

# Speed up raster processes
rasterOptions(chunksize = 1e+08, maxmemory = 1e+09)

# Allow for command line arguments
args = commandArgs(trailingOnly=TRUE)



####################
##### Controls #####
####################

# Name of project directory
projDir <- "EastCoast"

# Names of fields to model in SpeciesData shapefile
# If using command args separate species names by "-"
# strsplit(args[2], split="-")[[1]]
sppNames <- "Pres"

# Position of legend in exported maps
# Options include "bottomleft", "bottomright", "topleft", "topright"
legendPos = "bottomright"

# Number of CV folds (typically 5 or 10)
numFolds  <- 5

# Categorical predictor variables in environmental data
# NULL if none exist
facVars <- NULL

# Type of input environmental data (polygon or raster)
inEnvType <- "raster"

# Remove collinear predictors from environmental data  (VIF method)
rmCollPred <- FALSE

# Name of environmental data polygon feature class in geodatabase
fc <- "BoP"

# Fields to remove from polygon environmental data
remfields <- c( "Shape_Length", "Shape_Area", "BoPID" )

# Produce maps of predictor layers?
drawMaps <- TRUE



######################
##### Directories ####
######################

# Subfolders for Outputs
outdir <- file.path("Projects", projDir, "DataPrep")
outFigs <- "Figures"
outMaps <- "EnvMaps"
outData <- "Data"
outTabs <- "Tables"
outSpatial <- "Spatial"

# Create the main directory, and subfolders
dir.create( outdir, recursive=TRUE )
dir.create( file.path(outdir, outFigs) )
dir.create( file.path(outdir, outFigs, outMaps) )
dir.create( file.path(outdir, outData) )
dir.create( file.path(outdir, outData, outTabs) )
dir.create( file.path(outdir, outData, outSpatial) )



############################
#######     Run      #######
############################

# Sink output to file
rout <- file( file.path(outdir, "inputData.log"), open="wt" )
sink( rout, split = TRUE ) # display output on console and send to log file
sink( rout, type = "message" ) # send warnings to log file
options(warn=1) # print warnings as they occur

# Start the timer
sTime <- Sys.time( )


# Grab the environmental layers
enviroLayers <- GetEnviroLayers( layer = fc, # name of feature class in geodatabase
                                 remfields = remfields ) # fields to remove
# Get species data
spdat <- GetSppData( rmRareSpp = FALSE, # Remove rare species (based on species prevalence)
                     rareThresh = 0.05,  # Minimum species prevalence
                     calcPA = FALSE, # Model presence/absence from density
                     calcRich = FALSE ) # Model species richness (includes 'rare' species)

# Extract environmental data for species observations
edat <- ExtractEnviroData( shp = spdat,
                           envDat = enviroLayers,
                           facVars = facVars )

# Grab obList and enviroDat from ExtractEnviroData() object
obsList <- edat[[1]]
enviroDat <- edat[[2]]

# Create spatial block CV folds (run once for each species)
cv.list <- lapply(obsList$spNames, CV, folds = numFolds, obs = obsList, env = enviroLayers)
names(cv.list) <- obsList$spNames

# Calculate collinearity between environmental predictors
VIFs <- CalcVIFs( dat = enviroDat@data,
                  VIFThresh = 10 ) # VIFThresh = Max predictor collinearity

# Calcuate the minimum convex polygon for observations
obsMCP <- MakeMCP( loc = obsList$spatDat )

# Get land data (study area)
land <- GetLandData( doThin=TRUE )

# Plot collinearity
PlotVIFs( dat=VIFs )

# Plot predictor correlation matrix
PlotPredictorPairs( preds = enviroDat@data )

# Plot histograms comparing enviro layers and extracted eviro data
PlotPredictorHists( preds = enviroDat@data, layers = enviroLayers, facVars = facVars)

# Plot histograms of Species data
PlotObsHists( obs = obsList$sppDat )

# Plot map of spatial blocks (once for each species)
MapObs( spp=obsList$spNames, pts = obsList$spatDat, rasterLayer = enviroLayers[[1]], legendPos=legendPos)

# Plot maps of environmental layers
if( drawMaps ) MapEnvLayers( layers = enviroLayers, facVars = facVars, legendPos=legendPos )



############################
#####   Export Data    #####
############################


# If removing collinear predictor(s)
if( rmCollPred ) {

  # Remove predictor(s) from Enviro Layers
  if( inEnvType == "polygon" ) {
    enviroLayers <- enviroLayers[!names(enviroLayers@data) %in% exPred]
  }
  if( inEnvType == "raster" ) {
    lay <- names(enviroLayers)[!names(enviroLayers) %in% exPred]
    enviroLayers <- subset(enviroLayers, lay)
  }
  # Predictor(s) removed from Extracted Enviro Data
  enviroDatNotUsed <- enviroDat[exPred]
  # Extracted Enviro Data with predictor(s) removed
  enviroDat <- enviroDat[!names(enviroDat@data) %in% exPred]
  # Save Enviro Data
  save( enviroDatNotUsed, file=file.path(outdir, outData, "EnviroDatNotUsed.RData") )

  # Message
  cat( "\nRemoved ", length(exPred), " collinear predictor(s): ",
       paste(exPred, collapse=", "), "\n\n", sep="" )
} else if ( exists("exPred") ) {
  # Message
  cat( "\nRetained ", length(exPred), " collinear predictor(s): ",
       paste(exPred, collapse=", "), "\n\n", sep="" )
}


# Save environmental layers
save( enviroLayers, file=file.path(outdir, outData, "EnviroLayers.RData") )

# Save Enviro Data
save( enviroDat, file=file.path(outdir, outData, "EnviroDat.RData") )

# Export Extracted Enviro Data as csv
xyDat = data.frame( x = coordinates(enviroDat)[,1], y = coordinates(enviroDat)[,2] )
envdat <- data.frame( xyDat, enviroDat@data )
fileout <- file.path( outdir,outData, outTabs, "EnviroDat.csv" )
write.csv( envdat, file=fileout, row.names=FALSE )

# Export Extracted Enviro Data as shapefile
dsn <- file.path(outdir,outData, outSpatial)
writeOGR( enviroDat, dsn = dsn, layer = "EnviroDat", driver = "ESRI Shapefile", overwrite=TRUE )

# Save Species data
save( obsList, file=file.path(outdir, outData, "SpeciesDat.RData") )

# Save cv blocks
save( cv.list, file=file.path(outdir, outData, "cvBlocks.RData") )

# Export Species data as csv
dat <- data.frame( obsList$spatDat, obsList$sppDat )
fileout <- file.path( outdir, outData, outTabs, "SpeciesObs.csv" )
write.csv( dat, file=fileout, row.names=FALSE )

# Export Species data as shapefile
dsn <- file.path( outdir, outData, outSpatial )
coordinates( dat ) <- ~x+y
crs( dat ) <- CRS( geoCRS )
writeOGR( dat, dsn = dsn, layer = "SpeciesObs", driver = "ESRI Shapefile", overwrite=TRUE )

# Save Land and MC polygons
save( land, file=file.path(outdir, outData, "LandPolygon.RData") )
save( obsMCP, file=file.path(outdir, outData, "MCPolygon.RData") )

# Print end of file message and elapsed time
cat( "\nFinished: ", sep="" ); print( Sys.time( ) - sTime )

# Stop sinking output
sink( type = "message" )
sink( )
closeAllConnections()
